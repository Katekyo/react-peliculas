import { MapContainer, Marker, TileLayer, useMapEvent,Popup } from "react-leaflet"
import L from 'leaflet'
import icon from 'leaflet/dist/images/marker-icon.png'
import iconShadow from 'leaflet/dist/images/marker-shadow.png'
import {coordenadaDTO} from './coordenadas.model'
import { useState } from "react";
import 'leaflet/dist/leaflet.css';

let DefaultIcon = L.icon({
    iconUrl: icon,
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    shadowUrl: iconShadow,
    shadowSize: [68, 95],
    shadowAnchor: [22, 94]
});

L.Marker.prototype.options.icon = DefaultIcon;

export default function Mapa(props: mapaProps){
    const [coordenadas, setCoordenadas] = useState<coordenadaDTO[]>(props.coordenadas)
    return (
 
        <MapContainer
            center={[18.467455, -69.931242]} zoom={14}
            style={{height: props.height}}
        >
            <TileLayer attribution="React Películas"
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
                   {props.soloLectura ? null : 
                    <ClickMapa setPunto={coordenadas => {
                        setCoordenadas([coordenadas]);
                        props.manejarClickMapa(coordenadas);
                    }} />
                }
            
            {coordenadas.map(coordenada => <Marcador key={coordenada.lat + coordenada.lng} 
              {...coordenada}
            />)}
        </MapContainer>
    )
}

interface mapaProps {
    height: string;
    coordenadas: coordenadaDTO[];
    manejarClickMapa(coordenadas: coordenadaDTO): void;
    soloLectura: boolean;
}

Mapa.defaultProps = {
    height: '500px',
    soloLectura: false,
    manejarClickMapa:() => {}
}

function ClickMapa(props: clickMapaProps){
    useMapEvent('click', e => {
        props.setPunto({lat: e.latlng.lat, lng: e.latlng.lng})
    })
    return null;
}

interface clickMapaProps {
    setPunto(coordenadas: coordenadaDTO): void;
}

function Marcador(props: coordenadaDTO) {
    return (
        <Marker position={[props.lat, props.lng]}>
            {props.nombre ? <Popup>
                {props.nombre}
            </Popup> : null}
        </Marker>
    )
}