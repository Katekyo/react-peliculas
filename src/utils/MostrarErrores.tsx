export default function MostrarErrores(props: mostrarErrorProps){
    const style = {color: 'red'}
    console.log(props)
    return (
        <>
        {props.errores ? <ul style={style}>
            {props.errores.map((error, indice) => <li key={indice}>{error}</li>)}
        </ul> : null}
    </>
    )
}

interface mostrarErrorProps{
    errores: string[];
}