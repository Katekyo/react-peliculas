import axios, { AxiosResponse } from "axios";
import { useState, useEffect } from "react";
import { ReactElement } from "react-markdown"
import { Link } from "react-router-dom";
import Button from "./Button";
import confirmar from "./Confirmar";
import ListadoGenerico from "./ListadoGenerico";
import Paginacion from "./Paginacion";

export default function IndiceEntidad<T>(props: indiceEntidadProps<T>) {

    const [entidades, setEntidades] = useState<T[]>();
    const [totalDePaginas, setTotalDePaginas] = useState(0);
    const [RecordsPerPages, setRecordsPorPagina] = useState(10);
    const [page, setPagina] = useState(1);


  useEffect(() => {
    cargarDatos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, RecordsPerPages]);

  const cargarDatos = () => {
    axios.get(props.url, { params: { page,  RecordsPerPages } }).then((response: AxiosResponse<T[]>) => {
      const totalDePaginas =  parseInt(response.headers['entries'], 10)
      setTotalDePaginas(Math.ceil(totalDePaginas/RecordsPerPages))
      setEntidades(response.data);
    });
  }
  const remove = async (id: number) => {
      try {
        await axios.delete (`${props.url}/${id}`)
        cargarDatos();
      } catch (error) {
        console.log(error.response.data)
      }
  }

  const botones = (urlEditar: string, id: number) => 
      <>
        <Link className="btn btn-success" to={urlEditar}>Editar </Link>
         <Button className="btn btn-danger" onClick={(() => confirmar(() => remove(id)))} >Eliminar</Button>
      </>
  
    return (
        <>
      <h3>{props.titulo}</h3>
      <Link className="btn btn-primary" to={props.urlCrear}> Crear {props.nombreEntidad} </Link>
      <div className="form-group" style={{ width: '150px' }}>
         <label>Registro por pagina</label>
        <select 
          defaultValue={10}
          className='form-control' 
          onChange={e => 
          {
            setPagina(1)
            setRecordsPorPagina(parseInt(e.currentTarget.value,10))
          }
        } >
            <option value={5} >5</option>
            <option value={10} >10</option>
            <option value={25} >25</option>
            <option value={50} >50</option>
        </select>
       </div>
      <Paginacion cantidadTotalDePaginas={totalDePaginas} paginaActual={page} onChange ={nuevaPagina => setPagina(nuevaPagina)} />
      <ListadoGenerico listado={entidades}>
        <table className="table table-striped">
            {
                props.children(entidades!, botones)
            }
        </table>
      </ListadoGenerico>

        </>
    )
  }

  interface indiceEntidadProps<T>{
      url: string;
      urlCrear: string;
      children(entidades: T[], botonos: (urlEditar: string, id: number ) => ReactElement) : ReactElement;
      titulo: string;
      nombreEntidad: string;
  }