import "./App.css";
import Menu from "./utils/Menu";
import { Route, Switch } from "react-router";
import { BrowserRouter } from "react-router-dom";
import Rutas from "./route-config";
import configurarValidaciones from "./Validaciones";

configurarValidaciones()

function App() {
  return (
    <>
      <BrowserRouter>
        <Menu />
        <div className="container">
          <Switch>
            {Rutas.map((ruta) => (
              <Route key={ruta.path} path={ruta.path} exact={ruta.exact}>
                <ruta.componete />
              </Route>
            ))}
          </Switch>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
